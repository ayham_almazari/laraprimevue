<?php

namespace Mrmazari\LaraPrimeVueDataTables\factory;

use Mrmazari\LaraPrimeVueDataTables\Exceptions\NotExtendsFromHandlerContract;
use Mrmazari\LaraPrimeVueDataTables\FiltersHandlers\HandlerContract;
use ReflectionClass;

abstract class FilterFactory
{
    /**
     * @throws \ReflectionException
     */
    public static function make(string $filterName, array $filterData, object $queryBuilder): HandlerContract
    {
        $filterClass = new ReflectionClass($filterName);
        if ($filterClass->getParentClass()->getName() != HandlerContract::class) {
            throw new NotExtendsFromHandlerContract("Your Filter Handler Must Extends ". HandlerContract::class);
        }
        return $filterClass->newInstanceArgs([$filterData, $queryBuilder]);
    }
}