<?php
namespace Mrmazari\LaraPrimeVueDataTables\factory;

use Mrmazari\LaraPrimeVueDataTables\FiltersHandlers\BodyGlobalFiltersHandler;
use Mrmazari\LaraPrimeVueDataTables\FiltersHandlers\HandlerContract;
use Mrmazari\LaraPrimeVueDataTables\FiltersHandlers\QueryGlobalFiltersHandler;

class Filter extends FilterFactory
{

    /**
     * @throws \ReflectionException
     */
    public function params(array $filterData, object $queryBuilder): HandlerContract
    {
        return static::make(QueryGlobalFiltersHandler::class, $filterData, $queryBuilder);
    }


    /**
     * @throws \ReflectionException
     */
    public function body(array $filterData, object $queryBuilder): HandlerContract
    {
        return static::make(BodyGlobalFiltersHandler::class, $filterData, $queryBuilder);

    }
}