<?php

namespace Mrmazari\LaraPrimeVueDataTables\Exceptions;

use Illuminate\Http\Request;
use InvalidArgumentException;

class ColumnsDataTypesAttributeNotDefined extends InvalidArgumentException
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param Request $request
     *
     */
    public function render(Request $request): bool
    {
        report($this->getMessage());
        return false;
    }
}
