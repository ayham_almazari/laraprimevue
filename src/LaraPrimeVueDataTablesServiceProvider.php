<?php

namespace Mrmazari\LaraPrimeVueDataTables;

use Illuminate\Support\ServiceProvider;
use Mrmazari\LaraPrimeVueDataTables\factory\Filter;

class LaraPrimeVueDataTablesServiceProvider extends ServiceProvider
{

    public function register()
    {

    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->registerPublishes();
        }
        $this->app->singleton('LaraPrimeFilter', fn($app)=>new Filter());
    }

    protected function registerPublishes(): void
    {
        $this->publishes(
            [
                __DIR__ . '/../config/LaraPrimeVueDataTables.php' =>
                    config_path('LaraPrimeVueDataTables.php')
            ],
            'lara-prime-vue-data-tables-config'
        );
    }

}