<?php

namespace Mrmazari\LaraPrimeVueDataTables\FiltersHandlers;

use Carbon\Carbon;
use Mrmazari\LaraPrimeVueDataTables\Exceptions\ColumnsDataTypesAttributeNotDefined;
use ReflectionObject;
use Throwable;

abstract class HandlerContract implements HandlerInterface
{

    public function customWhere(string $column, mixed $value, string|null $matchMode): callable
    {
        return fn($query) => $query->{
            GlobalFilters::defaultCloseByColumnDataType(
                $this->getAttributesDefinedDataTypes()[$column]
            )
        }(
            column: $column,
            operator: GlobalFilters::defaultFilterOperator()[GlobalFilters::defaultFilters()[$matchMode]],
            value: GlobalFilters::defaultFilterOperatorRawMask(
                matchMode: GlobalFilters::defaultFilters()[$matchMode],
                value: $this->valueByFilter($value, $column)
            )
        );
    }

    public function valueByFilter(mixed $value, string $column): mixed
    {
        return match ($this->getAttributesDefinedDataTypes()[$column]) {
            "numeric", "text" => $value,
            "date" => Carbon::parse($value)->toDateString()
        };
    }

    abstract protected function getAttributesDefinedDataTypes(): array;

    /**
     * @throws Throwable
     */
    public function getDefinedAttributesDataTypesByReflection(object $queryBuilder)
    {
        $reflection = new ReflectionObject($queryBuilder);

        throw_unless(
            $reflection->getConstructor()->getAttributes(ColumnsDataTypes::class)[0] ?? false,
            new ColumnsDataTypesAttributeNotDefined("Construct Method Attribute " .
                ColumnsDataTypes::class . " Must Be Defined With Each Column Data Type ."));

        return $reflection->getConstructor()->getAttributes(ColumnsDataTypes::class)[0]
            ->newInstance()->getDataTypes();
    }

}
