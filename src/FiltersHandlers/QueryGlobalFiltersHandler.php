<?php

namespace Mrmazari\LaraPrimeVueDataTables\FiltersHandlers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\NoReturn;
use Mrmazari\LaraPrimeVueDataTables\Exceptions\ColumnsDataTypesAttributeNotDefined;
use ReflectionObject;
use Throwable;

class QueryGlobalFiltersHandler extends HandlerContract
{

    /**
     * @var array
     */
    private array $columnFilterTypes;

    /**
     * @throws ColumnsDataTypesAttributeNotDefined|Throwable
     */
    public function __construct(private readonly ?array $filters, public object $queryBuilder)
    {
         $this->columnFilterTypes= $this->getDefinedAttributesDataTypesByReflection($this->queryBuilder);
         $this->handle();
    }

    protected function getAttributesDefinedDataTypes(): array
    {
       return $this->columnFilterTypes;
    }

    public function handle(): void
    {
        foreach(
            $this->filters??[] as $column =>
        [
            'operator'     => $operator,
            'constraints'  => $constraints
        ]
        ):
            __declare:{
            $value1    = $constraints[0]['value'] ?? null;
            $matchMode1     =   $constraints[1]['matchMode'] ?? null;
            $value2    =   $constraints[2]['value'] ?? null;
            $matchMode2     =  $constraints[3]['matchMode'] ?? null;
        }

            if (empty($constraints)) {continue;}

            if (count($constraints)==2) {
                $this->queryBuilder->when(
                    !is_null($value1) && !is_null($matchMode1),
                    call_user_func([$this,'customWhere'], $column, $value1, $matchMode1)
                );
            }elseif (count($constraints)==4) {
                $this->queryBuilder
                    ->when(
                        !is_null($value1) && !is_null($matchMode1) && !is_null($value2) && !is_null($matchMode2),
                        fn($q)=>$q->where(
                            fn( $q)=>
                            $q->where(
                                call_user_func([$this,'customWhere'], $column, $value1, $matchMode1)
                            )->{GlobalFilters::defaultLogicalOperators()[$operator]}(
                                call_user_func([$this,'customWhere'], $column, $value2, $matchMode2)
                            )
                        )
                    );
            }
        endforeach;
    }
}
