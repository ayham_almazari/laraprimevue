<?php

namespace Mrmazari\LaraPrimeVueDataTables\FiltersHandlers;


use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
class ColumnsDataTypes
{
    public final const Numeric = "numeric";
    public final const Text = "text";
    public final const Date = "date";

    public function __construct(public array $dataTypes)
    {}

    public function getDataTypes(): array
    {
        return $this->dataTypes;
    }
}
