<?php

namespace Mrmazari\LaraPrimeVueDataTables\FiltersHandlers;

interface HandlerInterface
{
    public function customWhere(string $column, mixed $value, string $matchMode): callable;

    public function valueByFilter(mixed $value, string $column): mixed;

    public function handle(): void;

    public function getDefinedAttributesDataTypesByReflection(object $queryBuilder);

}