<?php

namespace Mrmazari\LaraPrimeVueDataTables\FiltersHandlers;

final class GlobalFilters
{
    final const DEFAULT_FILTERS = 'default-filters';
    final const DEFAULT_FILTER_OPERATOR = 'default-filter-operator';
    final const DEFAULT_LOGICAL_OPERATORS = 'default-logical-operators';
    final const DEFAULT_FILTER_OPERATOR_RAW_MASK = 'default-filter-operator-raw-mask';
    final const DEFAULT_CLOSE_BY_COLUMN_DATA_TYPE = 'default-close-by-column-data-type';


    final public static function checkIFConfigIsPublished(string $configKey, array $defaultValues): mixed
    {
        if ($configKey === self::DEFAULT_FILTER_OPERATOR_RAW_MASK ||
            $configKey === self::DEFAULT_CLOSE_BY_COLUMN_DATA_TYPE) {
            $config = config("LaraPrimeVueDataTables.$configKey");
            if (is_null($config)) {
                return $defaultValues['callback'](...$defaultValues['args']);
            } else {
                return $config(...$defaultValues['args']);
            }
        }
        return config("LaraPrimeVueDataTables.$configKey", $defaultValues);
    }

    final public static function defaultFilters(): array
    {
        return self::checkIFConfigIsPublished(self::DEFAULT_FILTERS, [
            //Text DataType Filters
            "startsWith" => "STARTS_WITH",
            "contains" => "CONTAINS",
            "notContains" => "NOT_CONTAINS",
            "endsWith" => "ENDS_WITH",
            "equals" => "EQUALS",
            "notEquals" => "NOT_EQUALS",
            //Numeric DataType Filters
            'lt' => 'LESS_THAN',
            'lte' => 'LESS_THAN_OR_EQUAL_TO',
            'gt' => 'GREATER_THAN',
            'gte' => 'GREATER_THAN_OR_EQUAL_TO',
            //Date DataType Filters
            'dateIs' => 'DATE_IS',
            'dateIsNot' => 'DATE_IS_NOT',
            'dateBefore' => 'DATE_BEFORE',
            'dateAfter' => 'DATE_AFTER',
        ]);
    }

    final public static function defaultFilterOperator(): array
    {
        return self::checkIFConfigIsPublished(self::DEFAULT_FILTER_OPERATOR, [
            //Text DataType Filters
            "STARTS_WITH" => "LIKE",
            "CONTAINS" => "LIKE",
            "NOT_CONTAINS" => "NOT LIKE",
            "ENDS_WITH" => "LIKE",
            "EQUALS" => "=",
            "NOT_EQUALS" => "<>",
            //Numeric DataType Filters
            'LESS_THAN' => "<",
            'LESS_THAN_OR_EQUAL_TO' => "<=",
            'GREATER_THAN' => ">",
            'GREATER_THAN_OR_EQUAL_TO' => ">=",
            //Date DataType Filters
            'DATE_IS' => "=",
            'DATE_IS_NOT' => "<>",
            'DATE_BEFORE' => "<",
            'DATE_AFTER' => ">",
        ]);
    }

    final public static function defaultLogicalOperators(): array
    {
        return self::checkIFConfigIsPublished(self::DEFAULT_LOGICAL_OPERATORS, [
            "and" => "where",
            "or" => "orWhere"
        ]);
    }

    final public static function defaultFilterOperatorRawMask(string $matchMode, mixed $value): string
    {
        return self::checkIFConfigIsPublished(
            self::DEFAULT_FILTER_OPERATOR_RAW_MASK,
            [
                'callback' => function (string $matchMode, mixed $value) {
                    return match ($matchMode) {
                        "NOT_CONTAINS", "CONTAINS" => "%$value%",
                        "STARTS_WITH" => "$value%",
                        "ENDS_WITH" => "%$value",
                        default => $value
                    };
                },
                'args' => func_get_args()
            ]
        );
    }

    final public static function defaultCloseByColumnDataType(string $dataType): string
    {
        return self::checkIFConfigIsPublished(
            self::DEFAULT_CLOSE_BY_COLUMN_DATA_TYPE,
            [
                'callback' => function (string $dataType): string {
                    return match ($dataType) {
                        ColumnsDataTypes::Numeric, ColumnsDataTypes::Text => "where",
                        ColumnsDataTypes::Date => "whereDate",
                        default => "where"
                    };
                },
                'args' => func_get_args()
            ]
        );
    }

}
