<?php

namespace Mrmazari\LaraPrimeVueDataTables\Search;

interface Searchable
{
   static function searchableColumns():array;
}
