<?php

namespace Mrmazari\LaraPrimeVueDataTables\Facades;

use Illuminate\Support\Facades\Facade;

class LaraPrimeFilter extends Facade
{

    protected static function getFacadeAccessor(): string
    {
        return 'LaraPrimeFilter';
    }
}