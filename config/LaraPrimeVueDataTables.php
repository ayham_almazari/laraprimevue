<?php

use Mrmazari\LaraPrimeVueDataTables\FiltersHandlers\ColumnsDataTypes;

return [

    'default-filters' => [
        //Text DataType Filters
        "startsWith" => "STARTS_WITH",
        "contains" => "CONTAINS",
        "notContains" => "NOT_CONTAINS",
        "endsWith" => "ENDS_WITH",
        "equals" => "EQUALS",
        "notEquals" => "NOT_EQUALS",
        //Numeric DataType Filters
        'lt' => 'LESS_THAN',
        'lte' => 'LESS_THAN_OR_EQUAL_TO',
        'gt' => 'GREATER_THAN',
        'gte' => 'GREATER_THAN_OR_EQUAL_TO',
        //Date DataType Filters
        'dateIs' => 'DATE_IS',
        'dateIsNot' => 'DATE_IS_NOT',
        'dateBefore' => 'DATE_BEFORE',
        'dateAfter' => 'DATE_AFTER',
    ],

    'default-filter-operator' => [
        //Text DataType Filters
        "STARTS_WITH" => "LIKE",
        "CONTAINS" => "LIKE",
        "NOT_CONTAINS" => "NOT LIKE",
        "ENDS_WITH" => "LIKE",
        "EQUALS" => "=",
        "NOT_EQUALS" => "<>",
        //Numeric DataType Filters
        'LESS_THAN' => "<",
        'LESS_THAN_OR_EQUAL_TO' => "<=",
        'GREATER_THAN' => ">",
        'GREATER_THAN_OR_EQUAL_TO' => ">=",
        //Date DataType Filters
        'DATE_IS' => "=",
        'DATE_IS_NOT' => "<>",
        'DATE_BEFORE' => "<",
        'DATE_AFTER' => ">",
    ],

    'default-logical-operators' => [
        "and" => "where",
        "or" => "orWhere"
    ],

    'default-filter-operator-raw-mask' => function (string $matchMode, mixed $value) {
        return match ($matchMode) {
            "NOT_CONTAINS", "CONTAINS" => "%$value%",
            "STARTS_WITH" => "$value%",
            "ENDS_WITH" => "%$value",
            default => $value
        };
    },

    'default-close-by-column-data-type' => function (string $dataType): string {
        return match ($dataType) {
            ColumnsDataTypes::Numeric, ColumnsDataTypes::Text => "where",
            ColumnsDataTypes::Date => "whereDate",
            default => "where"
        };
    },

];